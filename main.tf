provider "aws" {
  region = "ap-south-1"
  access_key = ""
  secret_key = ""
}

// Adding a variable
variable "subnet_cidr_block" {
  description = "CIDR block for the subnet being created"
  type = string
}

// Step 1 Create a VPC
resource "aws_vpc" "main-vpc" {
  cidr_block = "10.0.0.0/16"

  tags = {
    Name = "main-vpc"
  }
}

//Step 2 create internet gateway
resource "aws_internet_gateway" "main-gateway" {
  vpc_id = aws_vpc.main-vpc.id

  tags = {
    Name = "main"
  }
}

//Step 3 Route table
resource "aws_route_table" "route-table" {
  vpc_id = aws_vpc.main-vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.main-gateway.id
  }
  route {
    ipv6_cidr_block = "::/0"
    gateway_id = aws_internet_gateway.main-gateway.id
  }

  tags = {
    Name = "custom-route-table"
  }
}


resource "aws_subnet" "subnet-1" {
  vpc_id = aws_vpc.main-vpc.id
  cidr_block = var.subnet_cidr_block
  availability_zone = "ap-south-1a"
  //This is the availability zone for the subnet
  tags = {
    Name = "Subnet-1"
  }
}


resource "aws_route_table_association" "route-table-association" {
  subnet_id = aws_subnet.subnet-1.id
  route_table_id = aws_route_table.route-table.id
}

//Security group for port access (443, 80 and 22)
resource "aws_security_group" "security-group-for-vpc" {
  name = "allow_web_traffic"
  description = "Allow web traffic inbound traffic"
  vpc_id = aws_vpc.main-vpc.id

  ingress {
    description = "Https traffic"
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]

  }
  ingress {
    description = "HTTP traffic"
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]

  }
  ingress {
    description = "SSH from VPC"
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]

  }
  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]

  }

  tags = {
    Name = "allow_web_traffic"
  }
}


resource "aws_network_interface" "web-server-network-interface" {
  subnet_id       = aws_subnet.subnet-1.id
  private_ips     = ["10.0.1.50"]
  security_groups = [aws_security_group.security-group-for-vpc.id]
}

// Needs Internet gateway to be present
// Reference the entire object for internet gateway
resource "aws_eip" "public-eip" {
  vpc                       = true
  network_interface         = aws_network_interface.web-server-network-interface.id
  associate_with_private_ip = "10.0.1.50"
  depends_on                = [aws_internet_gateway.main-gateway]
}

resource "aws_instance" "web-server-instance" {
  ami = "ami-0c1a7f89451184c8b"
  instance_type = "t2.micro"
  availability_zone = "ap-south-1a"
  key_name = "rootKP"
  //associate with network interface
  network_interface {
    device_index = 0
    network_interface_id = aws_network_interface.web-server-network-interface.id
  }

  user_data = <<-EOF
            #! /bin/bash
              sudo apt update -y
              sudo apt install apache2 -y
              sudo systemctl start apache2
              sudo su
              sudo echo "My First Web Server" > /var/www/html/index.html
              exit
              EOF
}

output "Server_public_ip" {
  value = aws_eip.public-eip.public_ip
}

output "server_pip" {
  value = aws_instance.web-server-instance.private_ip
}

output "server_id" {
  value = aws_instance.web-server-instance.id
}
